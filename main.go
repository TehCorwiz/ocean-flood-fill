package main

import "log"

//replaceAtIndex: replaces a given rune at the given index in the given string
func replaceAtIndex(in string, r rune, i int) string {
	return in[:i] + string(r) + in[i+1:]
}

/*findOcean: Performs a flood fill on the given 2d slice
   Assumptions:
   * twoDArray is not empty.
   * Every twoDArray[y] is not empty.
   * Every twoDArray[y] is the same length.
   * x and y are positive.
   * x and y are both within len(twoDArray[y]) and len(twoDArray) respectively.
   * If the element referenced is not a 'W' then we don't perform the search.

   If any of these situations are false we simple return leaving twoDArray untouched.
   I've implemented these bounds checking measures.

   From the test sets we could assume that some of these don't matter because...
   * Both test sets use positive coordinates within the target map.
   * Both test sets target a 'W' element, we don't have a test set that targets a 'L' element.
   * Both test sets have valid input.

   A note about this algorithm, right now there is some overlap and duplication of work. We're
   double-checking many elements and simply returning because the element is either a 'L' or no
   longer a 'W'. As written we're not caching which coordinates have been checked. That could result in a
   50% reduction in workload compared to the naive version. Although the cost of a cache and lookup probably
   outweighs simply checking the element and returning. It would be worth profiling.
*/
func findOcean(twoDArray []string, y int, x int) {
	// twoDArray is not empty
	if len(twoDArray) == 0 {
		return
	}
	// Every twoDArray[y] is not empty
	// AND every twoDArray[y] is the same length
	firstLen := len(twoDArray[0])
	for _, s := range twoDArray {
		if (len(s) <= 0) || (len(s) != firstLen) {
			return
		}
	}
	// x and y are positive
	// AND x and y are both within len(twoDArray) and len(twoDArray[y]) respectively
	if (0 > y) && (y < len(twoDArray)) && (0 > x) && (x < len(twoDArray[0])) {
		return
	}

	//Check if current element is 'W' and mark it 'O'
	//Then search around the area
	if twoDArray[y][x] == 'W' {
		twoDArray[y] = replaceAtIndex(twoDArray[y], 'O', x)

		if x > 0 {
			leftX := x - 1
			findOcean(twoDArray, y, leftX)
		}

		if x < (len(twoDArray[0]) - 1) {
			rightX := x + 1
			findOcean(twoDArray, y, rightX)
		}
		if y > 0 {
			downY := y - 1
			findOcean(twoDArray, downY, x)
		}

		if y < (len(twoDArray) - 1) {
			upY := y + 1
			findOcean(twoDArray, upY, x)
		}
	}
}

func main() {
	/* Because we're defining this in terms of rows our coordinates are reversed.
	   Y = ocean[y] and X = ocean[Y][X]
	   Since in Golang strings are slices by default we can address individual elements by their index.
	   That makes this analogous to a 2d array.
	   Map is a reserved work in Golang, so I'm calling these oceans.
	*/
	ocean := []string{
		"LLLLLLLLWW",
		"LLLLLLLWWW",
		"WWWLLLLLWW"}
	log.Println(ocean)

	//I'm assuming this is Y,X because the input set Y is max 2
	startY := 0
	startX := 9

	findOcean(ocean, startY, startX)
	log.Println(ocean)

	ocean = []string{
		"WWLWLWWWLL",
		"LWLLLWLWLL",
		"LWWWWWLWWW"}
	startY = 0
	startX = 0
	log.Println(ocean)

	findOcean(ocean, startY, startX)
	log.Println(ocean)
}
